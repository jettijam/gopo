package main

func factory( svcName string ) Service{
    switch(svcName){
    case "tolower":
        return lowerService();
    case "toupper":
        return upperService();
    }
    return nil;
}
