package main
import (
    "strings"
)
type ToLowerClass struct{
}

func lowerService() *ToLowerClass{
    tuc := ToLowerClass{}
//    tuc.result = strings.ToLower(msg);
    return &tuc;
}

func (ls ToLowerClass) service(msg string) string{
	return strings.ToLower(msg);
}
