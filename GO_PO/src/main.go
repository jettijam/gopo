package main
import (
    "fmt"
    "net/http"
	"time"
    )
func main(){
    fmt.Println("Hello world")
    http.Handle("/", new(testHandler))
    http.ListenAndServe(":5000", nil)

}

type testHandler struct {
    http.Handler
}

func (h *testHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	//fmt.Printf("URL: %#v\n\nQuery: %#v\n\nname: %s\n",
    //req.URL,
    //req.URL.Query(),
    //req.URL.Query().Get("msg"))
	fmt.Println("incoming url : "+req.URL.Path);
	response := ""
    if req.URL.Path != "/service"{
	    return
    }

	svcName := req.URL.Query().Get("action");
	response = factory(svcName).service(req.URL.Query().Get("msg"));
    /*
	switch req.URL.Query().Get("action"){
	case "tolower":
		fmt.Println("tolower called")
		response = lowerService(req.URL.Query().Get("msg")).result
    case "toupper":
		fmt.Println("toupper called")
	    response = upperService(req.URL.Query().Get("msg")).result
	}
*/
	time.Sleep(1*time.Second);
	w.Write([]byte(response))
}
